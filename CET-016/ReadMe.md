# [CET-016] Task

## Description:
- It is required to build a new blue/green deployment once a new docker image is pushed to ECR

---
### Dockerfile for running httpd container
    FROM httpd

    COPY ./html/ /usr/local/apache2/htdocs/

    CMD ["httpd-foreground"]

---

### Steps to build and push docker image to ECR

#### **`1. aws ecr get-login-password --region us-east-1 --profile sherif-cet | sudo docker login --username AWS --password-stdin 639758934490.dkr.ecr.us-east-1.amazonaws.com`**
#### **`2. sudo docker build -t cet-016-httpd .`**
#### **`3. sudo docker tag cet-016-httpd:latest 639758934490.dkr.ecr.us-east-1.amazonaws.com/cet-016-httpd:latest`**
#### **`4. sudo docker push 639758934490.dkr.ecr.us-east-1.amazonaws.com/cet-016-httpd:latest`**

---

### Steps to create pipeline for an ECS blue/green deployment

#### **`1. Create image and push to an Amazon ECR repository`**
#### **`2. Create task definition and AppSpec source files and push to a CodeCommit repository`**
#### **`3. Create your Application Load Balancer and target groups`**
#### **`4. Create your Amazon ECS cluster and service`**
#### **`5. Create your CodeDeploy application and deployment group (ECS compute platform)`**
#### **`6. Create your pipeline`**
#### **`7. Make a change to your pipeline and verify deployment`**
