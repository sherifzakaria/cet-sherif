# [CET-014] Task

## Description:
- Application load balancer in the public subnet
- Auto-scaling group in a private subnet
- Use the packed image from [CET-007] and use user-data to create a simple HTML page to show “hello world - {instance id}”
- Scaling policy will increase instances when CPU utilization is more than 50%

---

### Steps to initiate terraform and provision the resources

#### **`1. terraform init`**
#### **`2. terraform plan`**
#### **`3. terraform plan`**
#### **`4. terraform apply --auto-aprove`**

---
### Steps to simulate cpu utilization

#### **`1. ssh into instances created by ASG`**</pre>
    ssh -i SherifPrivateKP.pem ec2-user@10.0.4.170 -o "proxycommand ssh -W %h:%p -i SherifKP.pem ec2-user@34.239.125.150"
    ssh -i SherifPrivateKP.pem ec2-user@10.0.3.70 -o "proxycommand ssh -W %h:%p -i SherifKP.pem ec2-user@34.239.125.150"
#### **`2. watch uptime`**
#### **`3. stress -c 100`**
