resource "aws_autoscaling_group" "cet-014-asg" {
  name = "cet-014-asg"
  launch_template {
    id      = aws_launch_template.cet-014-launch-template.id
    version = "$Latest"
  }
  vpc_zone_identifier = [aws_subnet.privateA.id, aws_subnet.privateB.id]
  min_size            = 2
  max_size            = 4
  target_group_arns   = [aws_lb_target_group.cet-014-tg.id]
  health_check_type   = "ELB"
  tag {
    key                 = "Name"
    value               = "terraform-asg-ec2"
    propagate_at_launch = true
  }
}



resource "aws_autoscaling_policy" "cet-014-sp" {
  name                   = "cet-014-sp"
  autoscaling_group_name = aws_autoscaling_group.cet-014-asg.name
  policy_type            = "TargetTrackingScaling"

  target_tracking_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ASGAverageCPUUtilization"
    }

    target_value = 50.0
  }
}
