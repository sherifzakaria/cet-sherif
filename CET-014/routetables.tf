resource "aws_route_table" "cet-014-public-rt" {
  vpc_id = aws_vpc.cet-014-vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.cet-014-igw.id
  }

  tags = {
    Name = "cet-014-public-rt"
  }
}

resource "aws_route_table" "cet-014-private-rt" {
  vpc_id = aws_vpc.cet-014-vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_nat_gateway.cet-014-nat.id
  }

  tags = {
    Name = "cet-014-private-rt"
  }
}

resource "aws_route_table_association" "rt-association-publicA" {
  subnet_id      = aws_subnet.publicA.id
  route_table_id = aws_route_table.cet-014-public-rt.id
}

resource "aws_route_table_association" "rt-association-publicB" {
  subnet_id      = aws_subnet.publicB.id
  route_table_id = aws_route_table.cet-014-public-rt.id
}

resource "aws_route_table_association" "rt-association-privateA" {
  subnet_id      = aws_subnet.privateA.id
  route_table_id = aws_route_table.cet-014-private-rt.id
}

resource "aws_route_table_association" "rt-association-privateB" {
  subnet_id      = aws_subnet.privateB.id
  route_table_id = aws_route_table.cet-014-private-rt.id
}