#!/bin/sh
yum update -y
yum install httpd -y
systemctl start httpd.service
systemctl enable httpd
export EC2_INSTANCE_ID=$(curl http://169.254.169.254/latest/meta-data/instance-id)
cat << EOF >> /var/www/html/index.html
<!DOCTYPE html>
<head>
</head>
<body>
    <h1>Hello World - $EC2_INSTANCE_ID</h1>
</body>
EOF
