resource "aws_security_group" "elb-sg" {
  name = "cet-014-elb-sg"

  description = "Allow HTTP traffic"
  vpc_id      = aws_vpc.cet-014-vpc.id
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

}

resource "aws_lb" "cet-014-alb" {
  name               = "cet-014-alb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.elb-sg.id]
  subnets = [
    aws_subnet.publicA.id,
    aws_subnet.publicB.id
  ]

  enable_cross_zone_load_balancing = true
}

resource "aws_lb_target_group" "cet-014-tg" {
  name     = "cet-014-tg"
  port     = 80
  protocol = "HTTP"
  vpc_id   = aws_vpc.cet-014-vpc.id
}

resource "aws_lb_listener" "cet-014-lb-listener" {
  load_balancer_arn = aws_lb.cet-014-alb.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.cet-014-tg.arn
  }
}
