resource "aws_subnet" "publicA" {
  vpc_id                  = aws_vpc.cet-014-vpc.id
  cidr_block              = "10.0.1.0/24"
  map_public_ip_on_launch = true
  availability_zone       = data.aws_availability_zones.available.names[0]


  tags = {
    Name = "cet-014-public-subnet-A"
  }
}

resource "aws_subnet" "publicB" {
  vpc_id                  = aws_vpc.cet-014-vpc.id
  cidr_block              = "10.0.2.0/24"
  map_public_ip_on_launch = true
  availability_zone       = data.aws_availability_zones.available.names[1]


  tags = {
    Name = "cet-014-public-subnet-B"
  }
}

resource "aws_subnet" "privateA" {
  vpc_id            = aws_vpc.cet-014-vpc.id
  cidr_block        = "10.0.3.0/24"
  availability_zone = data.aws_availability_zones.available.names[0]


  tags = {
    Name = "cet-014-private-subnet-A"
  }
}

resource "aws_subnet" "privateB" {
  vpc_id            = aws_vpc.cet-014-vpc.id
  cidr_block        = "10.0.4.0/24"
  availability_zone = data.aws_availability_zones.available.names[1]


  tags = {
    Name = "cet-014-private-subnet-B"
  }
}