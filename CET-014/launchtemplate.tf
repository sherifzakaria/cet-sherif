resource "aws_security_group" "cet-014-sg" {
  vpc_id      = aws_vpc.cet-014-vpc.id
  name        = "cet-014-private-instances-sg"
  description = "Allow HTTP traffic to instances through Elastic Load Balancer and SSH from Bastion Host"

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port       = 80
    to_port         = 80
    protocol        = "tcp"
    security_groups = [aws_security_group.elb-sg.id]
  }

  ingress {
    from_port       = 22
    to_port         = 22
    protocol        = "tcp"
    security_groups = [aws_security_group.bastion-host-sg.id]
  }
}

resource "aws_launch_template" "cet-014-launch-template" {
  name = "cet-014-launch-template"

  image_id = "ami-099186075e4357b5c"

  instance_type = "t2.micro"

  key_name = "SherifPrivateKP"

  vpc_security_group_ids = [aws_security_group.cet-014-sg.id]

  tag_specifications {
    resource_type = "instance"

    tags = {
      Name = "cet-014-instance"
    }
  }

  user_data = filebase64("./script.sh")
}
