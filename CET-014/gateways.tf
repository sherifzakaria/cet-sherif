resource "aws_internet_gateway" "cet-014-igw" {
  vpc_id = aws_vpc.cet-014-vpc.id

  tags = {
    Name = "cet-014-vpc"
  }
}

resource "aws_nat_gateway" "cet-014-nat" {

  depends_on = [aws_internet_gateway.cet-014-igw]

  allocation_id = aws_eip.cet-014-eip.id
  subnet_id     = aws_subnet.publicA.id

  tags = {
    Name = "cet-014-nat"
  }
}

resource "aws_eip" "cet-014-eip" {
  vpc      = true
}