data "aws_ami" "aws_linux" {
  most_recent = true

  filter {
    name   = "name"
    values = ["amzn2-ami-*-x86_64-gp2"]
  }

  owners = ["amazon"]
}

resource "aws_security_group" "bastion-host-sg" {
  vpc_id      = aws_vpc.cet-014-vpc.id
  name        = "cet-014-bastion-host-sg"
  description = "Allow SSH to Bastion Host"

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "bastion-host" {
  ami                    = data.aws_ami.aws_linux.id
  instance_type          = "t2.micro"
  key_name               = "SherifKP"
  vpc_security_group_ids = [aws_security_group.bastion-host-sg.id]
  subnet_id              = aws_subnet.publicA.id

  tags = {
    Name = "BastionHost-cet-014"
  }
}
