data "aws_availability_zones" "available" {
  state = "available"
}

resource "aws_vpc" "cet-014-vpc" {
  cidr_block           = "10.0.0.0/16"
  enable_dns_hostnames = true

  tags = {
    Name = "cet-014-vpc"
  }
}
