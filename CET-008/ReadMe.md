# [CET-008] Task

## Description
- Creating a bucket and sync folder using CLI.
- Create a cross-role to be accessed by another account

---

### AWS cli commands to create bucket and sync it with local folder

#### **`1. aws s3 mb s3://sherif-cet-008`**
#### **`2. aws s3 sync s3://sherif-cet-008 ./sherif-cet-008`**

---
### AWS cli commands to create cross account access role

#### **`1. aws iam create-role --role-name Sherif-CET-008-ListBuckets-Role --assume-role-policy-document file:///home/sherif/assume-role-policy.json`**

    {
        "Version": "2012-10-17",
        "Statement": {
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::912266772967:user/cet-assume-role-task"
            },
            "Action": "sts:AssumeRole"
        }
    }

#### **`2. aws iam put-role-policy --role-name Sherif-CET-008-ListBuckets-Role --policy-name cet-008 --policy-document file:///home/sherif/list-buckets-custom-policy.json`**


    {
        "Version": "2012-10-17",
        "Statement": {
            "Effect": "Allow",
            "Action": "s3:ListAllMyBuckets",
            "Resource": "arn:aws:s3:::*"
        }
    }


#### **`3. aws sts assume-role --role-arn arn:aws:iam::504727362876:role/Sherif-CET-008-ListBuckets-Role --role-session-name list-s3-buckets --profile cet-008-user`**

    export AWS_ACCESS_KEY_ID=ASIAXLBAYLU6AWBIWBXU

    export AWS_SECRET_ACCESS_KEY=0kvhbmRmr4dHvbdM8rtcaewLde5OPgHcTMT1luDY

    export AWS_SESSION_TOKEN=FwoGZXIvYXdzEN7//////////wEaDHtTVnPaIRKjPJfYTyKzAQDYX3nHSnB0S5i4OYZifIbaSyZFfZfaAxOIHK96FwsBRGEFN3daIcLDCGYLwuHbRr5pORfKM0T7KGYbrbzy70lnITm5IA/dmH+rPfUWRrKjnxS8s1oHftWUavxqfmHPKqeeiKc8r4Sy7OA2mOobL5M4z9vgKGd62FE9YUQtWR4f7+9kIyEYbivQzCbk1V3TNkqB+WB6OKMFIovDKJVy9+hsCiiFDhdA5UDkWBRc17M7CKCKKNv1wPcFMi2BK3vMK8TY3F9KdA0pDti+Rtq0bEUs5ekS0zlA8ISAAx+6PanEWbf0iu6ZHIc=

#### **`5. aws s3 ls`**
