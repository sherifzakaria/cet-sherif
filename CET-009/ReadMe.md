# [CET-009] Task

## Description
- Create Application and DeploymentGroup.
- Upload CodeDeploy scripts to s3
- Deploy applications on ec2. 

---

### **1. Added the following policies to a user:**

- AmazonS3FullAccess
- AWSCodeDeployFullAccess

### **2. Created application directory locally that contains the following:**

- appspec.yml
- index.html
- scripts

### **3. Ran the following commands to create application and upload it to S3**

- `aws deploy create-application --application-name sherif-webapp --profile cet-test`

- `aws deploy push --application-name ./sherif-webapp --s3-location s3://sherif-cet/webapp.zip --ignore-hidden-files --profile cet-test`

### **4. Launched EC2 instance with an attached role AmazonS3FullAccess and ran the following to install codedeploy agent:**

    sudo yum update
    sudo yum install ruby
    sudo yum install wget
    cd /home/ec2-user
    wget https://aws-codedeploy-us-east-1.s3.us-east-1.amazonaws.com/latest/install
    chmod +x ./install
    sudo ./install auto
    sudo service codedeploy-agent status

### **5. Created deployment group**

### **6. Run the following commands to upload and deploy any revision:**
- `aws deploy push --application-name ./sherif-webapp --s3-location s3://sherif-cet/webapp.zip --ignore-hidden-files --profile cet-test`

- `aws deploy create-deployment --application-name sherif-webapp --deployment-config-name CodeDeployDefault.OneAtATime --deployment-group-name SherifCD --s3-location bucket=sherif-cet,bundleType=zip,key=webapp.zip --profile cet-test`
