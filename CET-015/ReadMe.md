# [CET-015] Task

## Description:
- Create a Docker image that has Nginx installed from base centos image
- Push the image to ECR service
- Create an ECS cluster (only one EC2) and a task definition using the pushed image to show a simple welcome HTML page.

---
### Dockerfile to install Nginx from base Centos

    FROM centos

    RUN yum install -y nginx

    COPY ./html /usr/share/nginx/html

    EXPOSE 80

    CMD ["nginx", "-g", "daemon off;"]

---

### Steps to build and push docker image to ECR

#### **`1. aws ecr get-login-password --region us-east-1 --profile sherif-cet | sudo docker login --username AWS --password-stdin 639758934490.dkr.ecr.us-east-1.amazonaws.com`**
#### **`2. sudo docker build -t cet-015-nginx .`**
#### **`3. sudo docker tag cet-015-nginx:latest 639758934490.dkr.ecr.us-east-1.amazonaws.com/cet-015-nginx:latest`**
#### **`4. sudo docker push 639758934490.dkr.ecr.us-east-1.amazonaws.com/cet-015-nginx:latest`**

---

