# [CET-010] Task

## Description

It’s required to deploy a simple web app. When a developer uploads a new artifact to S3 bucket this should trigger a Lambda function that starts deployment on a specific deployment group, then sends SNS notification to an email with deployment details.

---

### **1. Creating the following terraform template files:**

- [provider.tf](./template/provider.tf)
- [roles.tf](./template/roles.tf)
- [instance.tf](./template/instance.tf)
- [codedeploy.tf](./template/codedeploy.tf)
- [bucket.tf](./template/bucket.tf)
- [lambda.tf](./template/lambda.tf)
- [sns.tf](./template/sns.tf)
- [variables.tf](./template/variables.tf)

### **2. Creating [python file](./template/sherif-cet-010-lambda.py) that contains the code for lambda function**
   
    import json
    import boto3

    def deploy(event, context):
    client = boto3.client('codedeploy')

    response = client.create_deployment(
        applicationName='Sherif-CET-010-App',
        deploymentGroupName='Sherif-CET010-DG',
        deploymentConfigName='CodeDeployDefault.OneAtATime',
        ignoreApplicationStopFailures=True,
        revision={
            'revisionType': 'S3',
            's3Location': {
                'bundleType': 'zip',
                'bucket': 'sherif-cet-010',
                'key': 'webapp.zip'
            }
        }
    )

    return {
        'statusCode': 200,
        'body': response
    }


### **3. Run the following commands to validate and build the template:**
- `terraform init`
- `terraform plan`
- `terraform apply -auto-approve`

### **4. Run the following command to upload deployment package to S3 bucket**
    aws deploy push --application-name Sherif-CET-010-App --s3-location s3://sherif-cet-010/webapp.zip --ignore-hidden-files --profile cet-test
