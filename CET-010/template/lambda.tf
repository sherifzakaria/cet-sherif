locals {
  lambda_zip_location = "output/function.zip"
}

data "archive_file" "function" {
  type        = "zip"
  source_file = "sherif-cet-010-lambda.py"
  output_path = "${local.lambda_zip_location}"
}

resource "aws_lambda_function" "codedeploy_lambda" {
  filename      = "${local.lambda_zip_location}"
  function_name = "sheirf-cet-010"
  role          = data.aws_iam_role.lambda_role.arn
  handler       = "sherif-cet-010-lambda.deploy"

  # source_code_hash = "${filebase64sha256(local.lambda_zip_location)}"

  runtime = "python3.8"

}

resource "aws_s3_bucket_notification" "bucket_notification" {
  bucket = "${aws_s3_bucket.b.id}"

  lambda_function {
    lambda_function_arn = "${aws_lambda_function.codedeploy_lambda.arn}"
    events              = ["s3:ObjectCreated:*"]
  }

  depends_on = [aws_lambda_permission.allow_bucket]
}

resource "aws_lambda_function_event_invoke_config" "sns_mail_notifications" {
  function_name = "sheirf-cet-010"

  destination_config {
    #   on_failure {
    #     destination = aws_sqs_queue.example.arn
    #   }

    on_success {
      destination = "${aws_sns_topic.deployment_updates.arn}"
    }
  }
}
