data "aws_iam_role" "lambda_role" {
  name = "Sherif-Lambda-Role"
}

data "aws_iam_role" "EC2_S3_Access" {
  name = "Sherif-CET-009-EC2-S3-Access"
}

resource "aws_iam_instance_profile" "webserver" {
  name = "codedeploy-instance-profile"
  role = data.aws_iam_role.EC2_S3_Access.name
}
