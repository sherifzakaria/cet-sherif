resource "aws_codedeploy_app" "webapp" {
  name = var.codedeploy_name
}

# create a deployment group
resource "aws_codedeploy_deployment_group" "webapp" {
  app_name              = "${aws_codedeploy_app.webapp.name}"
  deployment_group_name = "Sherif-CET010-DG"
  service_role_arn      = "arn:aws:iam::504727362876:role/Sherif-CET-009-CodeDeploy"

  deployment_config_name = "CodeDeployDefault.OneAtATime" # AWS defined deployment config

  ec2_tag_filter {
    key   = "Name"
    type  = "KEY_AND_VALUE"
    value = "Sherif-CET-010"
  }

}
