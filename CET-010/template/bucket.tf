# create an S3 bucket
resource "aws_s3_bucket" "b" {
  bucket = var.s3bucket_name
  acl    = "private"
}

resource "aws_lambda_permission" "allow_bucket" {
  statement_id  = "AllowExecutionFromS3Bucket"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.codedeploy_lambda.arn
  principal     = "s3.amazonaws.com"
  source_arn    = "${aws_s3_bucket.b.arn}"
}
