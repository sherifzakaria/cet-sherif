data "aws_ami" "aws_linux" {
  most_recent = true

  filter {
    name   = "name"
    values = ["amzn2-ami-*-x86_64-gp2"]
  }

  owners = ["amazon"]
}

resource "aws_instance" "webserver" {
  ami                  = data.aws_ami.aws_linux.id
  instance_type        = var.ec2_type
  key_name             = var.ec2_key
  iam_instance_profile = "${aws_iam_instance_profile.webserver.name}"
  vpc_security_group_ids = ["sg-03381540d645fc488"]
  user_data = <<EOF
    #! /bin/bash
    sudo yum update -y
    sudo yum install ruby -y
    sudo yum install wget -y
    cd /home/ec2-user
    wget https://aws-codedeploy-us-east-1.s3.us-east-1.amazonaws.com/latest/install
    chmod +x ./install
    sudo ./install auto
	EOF

  tags = {
    Name = "Sherif-CET-010"
  }
}
