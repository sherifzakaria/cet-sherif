import json
import boto3


def deploy(event, context):
    client = boto3.client('codedeploy')

    response = client.create_deployment(
        applicationName='Sherif-CET-010-App',
        deploymentGroupName='Sherif-CET010-DG',
        deploymentConfigName='CodeDeployDefault.OneAtATime',
        ignoreApplicationStopFailures=True,
        revision={
            'revisionType': 'S3',
            's3Location': {
                'bundleType': 'zip',
                'bucket': 'sherif-cet-010',
                'key': 'webapp.zip'
            }
        }
    )

    return {
        'statusCode': 200,
        'body': response
    }
