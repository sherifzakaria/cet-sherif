resource "aws_sns_topic" "deployment_updates" {
  name = "cet-010-deployment-updates-topic"
}

resource "aws_cloudformation_stack" "sns-subscription" {
  name = "cet-010-sns-subscription"

  parameters = {
    MySNSTopic = "${aws_sns_topic.deployment_updates.arn}"
  }

  template_body = <<STACK
{
    "AWSTemplateFormatVersion": "2010-09-09",
    "Parameters" : {
        "MySNSTopic" : {
            "Type" : "String"
        }
    },
    "Resources": {
        "MySubscription": {
            "Type": "AWS::SNS::Subscription",
            "Properties": {
                "Endpoint": "sherif.zakaria@rackspace.com",
                "Protocol": "email",
                "TopicArn": {
                    "Ref": "MySNSTopic"
                }
            }
        }
    }
}
STACK
}
