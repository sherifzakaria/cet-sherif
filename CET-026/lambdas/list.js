'use strict';

const AWS = require('aws-sdk');
const dbqueries = require('dbqueries');

module.exports.list = async (event, context) => {
  // fetch all from the database

  let scanResult = {}

  try {
    let tableName = process.env.DYNAMODB_TABLE
    scanResult = await dbqueries.list(tableName)
  } catch (error) {
    console.log('There was a problem scanning the countries')
    console.log('scanError', error)
    return {
      statusCode: 500
    }
  }

  if (scanResult.Items === null || !Array.isArray(scanResult.Items) || scanResult.Items.length === 0) {
    console.log('No items found')
    return {
      statusCode: 404
    }
  }

  return {
    statusCode: 200,
    body: JSON.stringify(scanResult.Items.map(item => {
      return {
        name: item.country,
        continent: item.continent
      }
    }))
  }
}
