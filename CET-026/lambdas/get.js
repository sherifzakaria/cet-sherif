'use strict';

const AWS = require('aws-sdk');
const dbqueries = require('dbqueries');

module.exports.get = async (event, context) => {

  if (typeof event.pathParameters.name !== 'string') {
    console.log('Validation Failed');
    return {
      statusCode: 400,
      body: JSON.stringify({
        "message": "bad request"
      })
    }
  }

  let getResult = {}

  try {
    let tableName = process.env.DYNAMODB_TABLE
    let countryName = decodeURIComponent(event.pathParameters.name.toLowerCase())
    getResult = await dbqueries.get(tableName, countryName)
  } catch (error) {
    console.log('There was a problem getting the country')
    console.log('getError', error)
    return {
      statusCode: 500
    }
  }

  if (getResult.Item === null) {
    return {
      statusCode: 404
    }
  }

  return {
    statusCode: 200,
    body: JSON.stringify({
      name: getResult.Item.country,
      age: getResult.Item.continent
    })
  }
};
