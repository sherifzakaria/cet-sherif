'use strict';

const AWS = require('aws-sdk');
const dbqueries = require('dbqueries');

module.exports.create = async (event, context) => {

  let bodyObj = {}

  try {
    bodyObj = JSON.parse(event.body)
  } catch (jsonError) {
    console.error('There was an error parsing the body', jsonError)
    return {
      statusCode: 400,
      body: JSON.stringify({
        "message": "bad request"
      })
    }
  }

  if (typeof bodyObj.country !== 'string') {
    console.log('Validation Failed');
    return {
      statusCode: 400,
      body: JSON.stringify({
        "message": "bad request"
      })
    }
  }

  let item = {
    country: bodyObj.country.toLowerCase(),
    continent: bodyObj.continent.toLowerCase()
  }

  try {
    let tableName = process.env.DYNAMODB_TABLE
    await dbqueries.create(item, tableName)

  } catch (error) {
    console.error('There was a problem putting the country')
    console.error('putError', error)
    return {
      statusCode: 500,
      body: JSON.stringify({
        "message": "Coudn't add country"
      })
    }
  }

  return {
    statusCode: 201,
    body: JSON.stringify({
      "item": item
    })
  }

};
