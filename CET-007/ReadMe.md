# [CET-007] Task

### **1. Created packer template with the following sections:**

- Variables
- Builders
- description
- Provisioners

### **2. Created variables json file**

### **3. Run the following commands to validate and build the template:**
- `packer validate packer-template.json`
- `packer build -var-file variables.json packer-template.json`

### **4. Run the following Powershell command to instantiate an instance from the create AMI**
    
    New-EC2Instance   
    Region us-east-1 
    ImageId ami-098aecc2aa44f3769 
    MinCount 1 -MaxCount 1 
    KeyName sherif-us-east-1-keypair
    SecurityGroupId sg-03381540d645fc488
    InstanceType t2.micro
    SubnetId subnet-e9f10a8e
