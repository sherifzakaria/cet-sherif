# [CET-005] Task

## First CloudFormation Template

### **1. Created parameters section with the following parameters:**

- VPC CIDR 
- Public Subnet A CIDR
- Public Subnet B CIDR
- Private Subnet A CIDR
- Private Subnet B CIDR

### **2. Created Resources section with the following resource types:**

1. VPC

2. Public Subnet A

3. Public Subnet B

4. Private Subnet A

5. Private Subnet B

6. Public Route Table

7. Private Route Table

8. Internet Gateway ( associated it to public subnets only )

9. NAT Gateway ( associated it to private subnets only )

10. Elastic IP Address to associated with NAT Gateway

11. Network ACL with the below allowed entries only:
    - SSH 22
    - HTTP 80
    - HTTPS 443
    - Ephermal ports 1024 - 65535

### **3. Created Output section with the following parameter types:**

1. VPC ID
2. VPC CIDR
3. Subnet IDs

---

## Second CloudFormation Template

### **1. Created parameters section with the following parameters:**

- VPC Stack Name

### **2. Created Resources section with the following resource types:**

1. Four EC2 instance for web servers each in different AZ

2. EC2 instance to be used as bastion host

3. Three security groups:
    - BastionSecurityGroup
        * Allows only port 22 from 41.44.93.76/32
    - PublicSecurityGroup
        * Allows port 80 and 443 from anywhere
        * Allows port 22 from VPC CIDR only
    - PrivateSecurityGroup
        * Allows port 80, 443 and 22 from VPC CIDR only