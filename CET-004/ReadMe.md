# [CET-004] Task

### **1. Created parameters section with the following parameters:**

- IndexHTML 
- BucketName
- InstanceTypeParameter
- InstanceName
- SubnetID
- LatestAmiId

### **2. Created Resources section with the following resource types:**

1. EC2 instance

    - CFN template creates EC2 instance with configurations using template parameters
    - User data using cloudformation helper scripts is added to install httpd and download html file from S3 bucket

2. IAM Role
    
    - IAM role is used to give grant S3 bucket readonly access to EC2 instances.

3. Instance Profile
    
    - Instance profile is used to pass role information to the EC2 instance when the instance starts.

4. Two security groups

    - **SSHSecurityGroup** is used to allow user to ssh the EC2 instance

    - **WebSecurityGroup** is used to allow http and https requests to the EC2 instance
    