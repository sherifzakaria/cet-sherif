# [CET-011] Task

## Description
- Create a Web API that List/save/get countries in/from DynamoDB.
- Using the Custom resource to initially fill DynamoDB

---
### 1. Creating serverless template

`serverless create --template aws-nodejs`

### 2. Updating [template](./serverless.yml) with the required services

### 3. Adding the following lambda files:
* [list.js](./countries/list.js)
* [create.js](./countries/create.js)
* [get.js](./countries/get.js)

### 4. Using custom resources to initially fill DynamoDB:

    const AWS = require("aws-sdk");
    const response = require("cfn-response");
    const docClient = new AWS.DynamoDB.DocumentClient();
    exports.handler = function(event, context) {
        console.log(JSON.stringify(event,null,2));
        var params = {
          RequestItems: {
            "cet-011-countries-dev": [
              {
                PutRequest: {
                  Item: {
                    "country": "germany",
                    "continent": "europe"
                  }
                }
              },
              {
                PutRequest: {
                  Item: {
                    "country": "italy",
                    "continent": "europe"
                  }
                }
              }
            ]
          }
        };
    docClient.batchWrite(params, function(err, data) { if (err) {
      response.send(event, context, "FAILED", {});
    } else {
      response.send(event, context, "SUCCESS", {});
    }
    });
    };

### 5. Deploy using serverless
  `serverless deploy --template aws-nodejs`
