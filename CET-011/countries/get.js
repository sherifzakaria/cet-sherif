'use strict';

const AWS = require('aws-sdk');

const dynamoDb = new AWS.DynamoDB.DocumentClient();

module.exports.get = (event, context, callback) => {

  let errorMessage = {
    message: 'Couldn\'t fetch the country.'
  }



  const params = {
    TableName: process.env.DYNAMODB_TABLE,
    Key: {
      country: decodeURIComponent(event.pathParameters.country.toLowerCase()),
    },
  };

  // fetch from the database
  dynamoDb.get(params, (error, result) => {
    // handle potential errors
    if (error) {
      console.error(error);
      callback(null, {
        statusCode: error.statusCode || 501,
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(errorMessage),
      });
      return;
    }

    if (!result.Item) {

      const response = {
        statusCode: 404,
        body: JSON.stringify(errorMessage),
      };
      callback(null, response);

    } else {
      // create a response
      const response = {
        statusCode: 200,
        body: JSON.stringify(result.Item),
      };
      callback(null, response);

    }
  });
};
