'use strict';

const AWS = require('aws-sdk');

const dynamoDb = new AWS.DynamoDB.DocumentClient();
const params = {
  TableName: process.env.DYNAMODB_TABLE,
};

module.exports.list = async (event, context, callback) => {
  // fetch all from the database
  try {
    const result = await dynamoDb.scan(params).promise();

    return {
      statusCode: 200,
      body: JSON.stringify(result.Items)
    }
  } catch (error) {
    return {
      statusCode: 501,
      headers: { 'Content-Type': 'text/plain' },
      body: JSON.stringify('Couldn\'t fetch countries.'),
    };
  }
};
