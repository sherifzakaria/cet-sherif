import json
import boto3


def lambda_handler(event, context):

    client = boto3.client('s3')
    response = client.put_bucket_encryption(
    Bucket = event['S3BucketName'],
    ServerSideEncryptionConfiguration={
        'Rules': [
            {
                'ApplyServerSideEncryptionByDefault': {
                    'SSEAlgorithm': 'AES256'
                }
            },
        ]
    })
    
    return response
